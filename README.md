# Procrastination Avoidinator By Ondra

## Usage

```bash
git clone git@gitlab.com:hajnyon/pabo.git
```

In Chrome: `Manage Extensions` -> `Load unpacked` -> Select cloned folder -> Enable extension

Try to go to **9gag.com** -> should be overlaid.

## Adding/removing urls

Edit array in `manifest.json` under `content_scripts.matches`
Reload extension from `Manage extensions` in chrome after.

Blog post: https://blog.hajnyon.cz/you-have-no-power-here/
